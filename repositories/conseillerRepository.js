const { client } = require('./conn_postgre');

async function verifConseiller(username, password) {
	return (
		(
			await client.query(
				'SELECT * FROM conseiller WHERE username = $1::text AND password = $2::text',
				[username, password]
			)
		).rows.length != 0
	);
}

async function insertConseiller(username, password) {
	return (
		await client.query(
			'INSERT INTO conseiller VALUES($1::text, $2::text)',
			[username, password]
		)
	).rows;
}

module.exports = { verifConseiller, insertConseiller };
