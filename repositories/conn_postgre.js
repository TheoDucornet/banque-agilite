const { Client } = require('pg')
const client = new Client({
    user: "username",
    password: "password",
    host: "localhost",
    post: "5432",
    database: "default_database"
})
client.connect()

module.exports = { client }