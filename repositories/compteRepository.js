const {client} = require("./conn_postgre");

async function insertCompte(username, solde) {
    return (await client.query('INSERT INTO compte (username,solde) VALUES($1::text, $2::numeric)', [username, solde])).rows
}
module.exports = { insertCompte }