const {client} = require("./conn_postgre");

async function verifClient(username) {
    return (await client.query('SELECT * FROM client WHERE username = $1::text ', [username])).rows.length != 0
}

async function insertClient(username, password) {
    return (
        await client.query(
            'INSERT INTO client VALUES($1::text, $2::text)',
            [username, password]
        )
    ).rows;
}


module.exports = { verifClient, insertClient }