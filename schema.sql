DROP TABLE IF EXISTS compte;
DROP TABLE IF EXISTS conseiller;
DROP TABLE IF EXISTS client;

CREATE TABLE conseiller
(
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    CONSTRAINT PK_conseiller PRIMARY KEY (username)
);

INSERT INTO conseiller VALUES
    ('admin', 'admin');


CREATE TABLE client
(
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    CONSTRAINT PK_client PRIMARY KEY (username)
);

INSERT INTO client VALUES
    ('client', 'client');



CREATE TABLE compte
(
    id INTEGER GENERATED ALWAYS AS IDENTITY,
    solde DECIMAL(10, 2) NOT NULL,
    username VARCHAR(50) NOT NULL REFERENCES client(username),
    CONSTRAINT PK_compte PRIMARY KEY (id)
);

CREATE TABLE virement
(
    id INTEGER GENERATED ALWAYS AS IDENTITY,
    amount DECIMAL(10, 2) NOT NULL,
    account_from   INTEGER NOT NULL REFERENCES compte(id),
    account_to     INTEGER NOT NULL REFERENCES compte(id),
    CONSTRAINT PK_virement PRIMARY KEY (id)
);