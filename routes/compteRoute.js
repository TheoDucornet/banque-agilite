const conseillerRepository = require('../repositories/conseillerRepository')
const clientRepository = require('../repositories/clientRepository')
const compteRepository = require('../repositories/compteRepository')
async function compteNew(req, res) {
    if (req.body.username == undefined || req.body.password == undefined || req.body.newSolde == undefined || req.body.newUsername == undefined) {
        res.status(400).send()
        return
    }
    try {
        if (await conseillerRepository.verifConseiller(req.body.username, req.body.password)) {
            if(await clientRepository.verifClient(req.body.newUsername)){
                res.status(200).send(await compteRepository.insertCompte(req.body.newUsername, req.body.newSolde))
            }

        } else {
            res.status(403).send()
        }
    } catch (err) {
        console.error(err)
        res.status(500).send()
    }
}

module.exports = { compteNew }