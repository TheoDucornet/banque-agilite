const conseillerRepository = require('../repositories/conseillerRepository')
const clientRepository = require('../repositories/clientRepository')

async function clientNew(req, res) {
    if (req.body.username == undefined || req.body.password == undefined || req.body.newClientPassword == undefined || req.body.newClientUsername == undefined) {
        res.status(400).send()
        return
    }
    try {
        if (await conseillerRepository.verifConseiller(req.body.username, req.body.password)) {
            res.status(200).send(await clientRepository.insertClient(req.body.newClientUsername, req.body.newClientPassword))
        } else {
            res.status(403).send()
        }
    } catch (err) {
        console.error(err)
        res.status(500).send()
    }
}

module.exports = { clientNew }