const virementRepository = require('../repositories/virementRepository');

async function virement(req, res) {
	if (
		req.body.from == undefined ||
		req.body.to == undefined ||
		req.body.amount == undefined
	) {
		res.status(400).send();
		return;
	}
	try {
		res.status(200).send(
			await virementRepository.virement(
				req.body.from,
				req.body.tp,
				req.body.amount
			)
		);
	} catch (err) {
		console.error(err);
		res.status(500).send();
	}
}

module.exports = { virement };
