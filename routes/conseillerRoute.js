const conseillerRepository = require('../repositories/conseillerRepository')

async function conseillerAuth(req, res) {
    if (req.body.username == undefined || req.body.password == undefined) {
        res.status(400).send()
        return
    }
    try {
        res.status(200).send(await conseillerRepository.verifConseiller(req.body.username, req.body.password))
    } catch (err) {
        console.error(err)
        res.status(500).send()
    }
}

async function conseillerNew(req, res) {
    if (req.body.username == undefined || req.body.password == undefined || req.body.newPassword == undefined || req.body.newUsername == undefined) {
        res.status(400).send()
        return
    }
    try {
        if (await conseillerRepository.verifConseiller(req.body.username, req.body.password)) {
            res.status(200).send(await conseillerRepository.insertConseiller(req.body.newUsername, req.body.newPassword))
        } else {
            res.status(403).send()
        }
    } catch (err) {
        console.error(err)
        res.status(500).send()
    }
}

module.exports = { conseillerNew, conseillerAuth }