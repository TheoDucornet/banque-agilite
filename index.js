const express = require('express');
const app = express();
const server = require('http').createServer(app);
const cors = require('cors');
const bodyParser = require('body-parser');

const corsOptions = {
	origin: '*',
};

app.use(cors(corsOptions));

const port = 9876;

server.listen(port, (req, res) => {
	console.log('Connected on : ' + port);
});

app.get('/', (req, res) => {
	res.status(200).send('Hello World !');
});


const { conseillerAuth, conseillerNew } = require('./routes/conseillerRoute')
const {compteNew} = require("./routes/compteRoute");
const {clientNew} = require("./routes/clientRoute");
const { virement } = require('./routes/virementRoute');

app.post('/auth', bodyParser.json(), conseillerAuth)
app.post('/conseiller/new', bodyParser.json(), conseillerNew)
app.post('/client/new', bodyParser.json(), clientNew)
app.post('/compte/new', bodyParser.json(), compteNew)
app.post('/virement', bodyParser.json(), virement);

