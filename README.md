# Banque Agilité

## Lancer la base de donnée
Avec les logs :
```sh
docker compose up
```
Sans les logs (en détaché):
```sh
docker compose up -d
```

## Lancer l'API
```sh
npm run start
```